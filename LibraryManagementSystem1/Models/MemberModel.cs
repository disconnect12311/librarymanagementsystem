﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem1.Models
{
    public class MemberModel
    {
        public int Id { get; set; }
        public string StudentNum { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Middlename { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public string Department { get {
                if (DepartmentId == 1)
                {
                    return "Pre-Elementary";
                }
                else if (DepartmentId == 2)
                {
                    return "Elementary";
                }
                else if (DepartmentId == 3)
                {
                    return "Junior High School";
                }
                else if (DepartmentId == 4)
                {
                    return "Senior High School";
                }
                else if (DepartmentId == 5)
                {
                    return "College";
                }
                else
                {
                    return "";
                }

            } }
    }
}