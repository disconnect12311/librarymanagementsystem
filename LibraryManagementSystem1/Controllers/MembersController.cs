﻿using DBDriver;
using LibraryManagementSystem1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem1.Controllers
{
    public class MembersController : Controller
    {
        private LibraryManagementSystemWebBasedEntities db = new LibraryManagementSystemWebBasedEntities();

        // GET: Members
        public ActionResult Index()
        {
            ViewBag.members = db.Members.Select(x => new MemberModel {
                Firstname = x.Firstname,
                Lastname = x.Lastname,
                Middlename = x.Middlename,
                DepartmentId = x.DepartmentID,
                StudentNum = x.StudentNum
            });
            return View();
        }

        public async Task<ActionResult> SyncFromBCASOASIS()
        {
            using (var client = new HttpClient())
            {
                var json = await client
                    .GetStringAsync("http://jefjeffjeffff-001-site2.htempurl.com/api/enrolled-students");

                var members = JsonConvert.DeserializeObject<List<MemberModel>>(json);

                foreach (var memberFromApi in members)
                {
                    var existingRecord = db.Members.FirstOrDefault(x => x.StudentNum.ToUpper()
                        == memberFromApi.StudentNum.ToUpper());

                    if (existingRecord != null)
                    {
                        existingRecord.StudentNum = memberFromApi.StudentNum;
                        existingRecord.Firstname = memberFromApi.Firstname;
                        existingRecord.Lastname = memberFromApi.Lastname;
                        existingRecord.Middlename = memberFromApi.Middlename;
                        existingRecord.DepartmentID = memberFromApi.DepartmentId;
                    }
                    else
                    {
                        db.Members.Add(new Member
                        {
                            StudentNum = memberFromApi.StudentNum,
                            Firstname = memberFromApi.Firstname,
                            Lastname = memberFromApi.Lastname,
                            Middlename = memberFromApi.Middlename,
                            DepartmentID = memberFromApi.DepartmentId
                        });
                    }
                }

                db.SaveChanges();
            }

            return Json("SUCCESS", JsonRequestBehavior.AllowGet);
        }
    }
}